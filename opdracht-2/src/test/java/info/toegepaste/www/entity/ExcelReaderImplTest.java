/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.entity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author Jordy Dieltjens
 */
public class ExcelReaderImplTest extends TestCase {

    public ExcelReaderImplTest(String testName) {
	super(testName);
    }

    /*
    @Override
    protected void setUp() throws Exception {
	super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
	super.tearDown();
    }
     */
    
    /**
     * Test of readStudents method, of class ExelReaderImpl.
     */
    public void testReadStudents() throws FileNotFoundException {
	System.out.println("--- Studenten lezen ---");

	String path = Thread.currentThread().getContextClassLoader().getResource("testexcel.xlsx").getPath();
	File inputfile = new File(path);
        InputStream file = new FileInputStream(inputfile);
	ExcelReaderImpl instance = new ExcelReaderImpl();

	List<Student> studenten = instance.readStudents(file);

	// controleren of het object studeten gevuld wordt met de gegevens van het .xlsx bestand
	assertNotNull(studenten);
	assertEquals(2, studenten.size());
	assertEquals("Clinton", studenten.get(0).lastname);
    }
    
}
