<%-- 
    Document   : overzicht
    Created on : Oct 10, 2016, 9:46:59 AM
    Author     : Jordy Dieltjens
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Overzicht van nieuwe studenten</h1>
            
        <ul>
            
            <c:forEach var="student"  items="${studenten}">
                          <li><c:out value="${student.firstname}" /> <c:out value="${student.lastname}" />: <c:out value="${student.email}" /></li>
                        </c:forEach>
            
           
        </ul>
    </body>
</html>
