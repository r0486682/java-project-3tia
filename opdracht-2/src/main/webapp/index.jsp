<%-- 
	Thomas More opleiding Toegepaste Informatica
	Document   : index
	Created on : 3-okt-2016, 22:37:51
	Author     : 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Opdracht 2</title>
    </head>
    <body>
	<form action="ManageServlet" method="post" enctype='multipart/form-data'>
	    <h1>Opdracht 2 Excelbestand uploaden</h1>
	    <input type="file" required name="excelBestand"/>
	    
	    <p><button name="knopUploaden" id="knopUploaden">Bestand uploaden</button></p>
	</form>
    </body>
</html>

