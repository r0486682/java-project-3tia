/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.entity;

import java.io.InputStream;
import java.util.List;

/**
 *
 * @author Hendrik
 */
public interface ExcelReader {

    public List<Student> readStudents(InputStream file);
}