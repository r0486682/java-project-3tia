/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.ejb;

import info.toegepaste.www.entity.ExcelReaderImpl;
import info.toegepaste.www.entity.Student;
import java.io.InputStream;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jordy Dieltjens
 */
@Stateless
public class StudentService {

    public List<Student> leesStudenten(InputStream file){
            
        ExcelReaderImpl eri = new ExcelReaderImpl();
        
        List<Student> studenten = eri.readStudents(file);
        
          EntityManagerFactory emf = Persistence.createEntityManagerFactory("opdracht2PU");

        EntityManager em = emf.createEntityManager();
        try {
          em.getTransaction().begin();
                for (Student student : studenten) {
                    em.persist(student);
                }
                em.getTransaction().commit();
                 } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            em.close();
            emf.close();
        }
        return studenten;
        
    }
}
