/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www;

import info.toegepaste.www.beans.CryptoUtil;
import info.toegepaste.www.entity.Klas;
import info.toegepaste.www.entity.Resultaat;
import info.toegepaste.www.entity.Student;
import info.toegepaste.www.entity.Test;
import info.toegepaste.www.entity.Vak;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ejb.EJB;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 *
 *
 */
@WebServlet(name = "ManageServlet", urlPatterns = {"/ManageServlet"})
@MultipartConfig(location = "")
public class ManageServlet extends HttpServlet {

    CryptoUtil cryptoUtil = new CryptoUtil();
    String key = "ezeon8547";
    Client client = null;
    String base_url = "";

    @EJB
    //private StudentService studentService;
    //private ResultaatService resultaatService;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, Exception {

        URL url = new URL(request.getRequestURL().toString());
        base_url = url.getProtocol() + "://" + url.getHost() + ":" + url.getPort() + "/rest-project/rest/";
        RequestDispatcher rd = null;
        client = ClientBuilder.newClient();
        String loginBoodschap = "";
        HttpSession session = request.getSession();

        if (request.getParameter("inloggen") != null) {
            String email = request.getParameter("email");
            String pass = request.getParameter("wachtwoord");
            String encryptedPassword = client.target(base_url + "gebruiker/getPassFromUser/" + email).request(MediaType.TEXT_PLAIN).get(String.class);

            if (cryptoUtil.encrypt(key, pass).equals(encryptedPassword)) {
                session.setAttribute("isIngelogd", true);
            } else {
                session.setAttribute("isIngelogd", false);
                loginBoodschap = "E-mailadres en wachtwoord komen niet overeen!";
            }
        } else if (request.getParameter("uitloggen") != null) {
            session.setAttribute("isIngelogd", false);
        }

        if ((Boolean) session.getAttribute("isIngelogd")) {
            if (request.getParameter("klastoegevoegd") != null) {

                Long id = Long.parseLong(request.getParameter("klastoegevoegd"));
                Klas klas = client.target(base_url + "klassen/getKlas/" + id).request(MediaType.APPLICATION_XML).get(new GenericType<Klas>() {
                });

                request.setAttribute("klas", klas);
                request.setAttribute("studenten", klas.getStudenten());
                rd = request.getRequestDispatcher("overzichtKlas.jsp");
            } else if (request.getParameter("lijstUploaden") != null) {
                rd = request.getRequestDispatcher("lijstUploaden.jsp");
            } else if (request.getParameter("rapportDownloaden") != null) {
                rd = toonPaginaRapportenDownloaden(request);
            } else if (request.getParameter("testresultaten") != null) {
                String testId = request.getParameter("testresultaten");
                List<Resultaat> resultaten = client.target(base_url + "resultaten/getAllByTest/" + testId).request(MediaType.APPLICATION_XML).get(new GenericType<List<Resultaat>>() {
                });
                Test test = client.target(base_url + "testen/getById/" + testId).request(MediaType.APPLICATION_XML).get(new GenericType<Test>() {
                });

                request.setAttribute("test", test);
                request.setAttribute("resultaten", resultaten);
                rd = request.getRequestDispatcher("testresultaten.jsp");
            } else if (request.getParameter("resultatenWijzigen") != null) {
                resultatenWijzigenOpvullen(request);

                rd = request.getRequestDispatcher("resultatenWijzigen.jsp");
            } else if (request.getParameter("resultatenStudent") != null) {
                String studentId = request.getParameter("keuzeStudent");
                List<Resultaat> resultaten = client.target(base_url + "resultaten/getAllFromStudent/" + studentId).request(MediaType.APPLICATION_XML).get(new GenericType<List<Resultaat>>() {
                });

                resultatenWijzigenOpvullen(request);

                request.setAttribute("resultaten", resultaten);
                rd = request.getRequestDispatcher("resultatenWijzigen.jsp");
            } else if (request.getParameter("resultatenTest") != null) {
                String testId = request.getParameter("keuzeTest");
                List<Resultaat> resultaten = client.target(base_url + "resultaten/getAllByTest/" + testId).request(MediaType.APPLICATION_XML).get(new GenericType<List<Resultaat>>() {
                });

                resultatenWijzigenOpvullen(request);

                request.setAttribute("resultaten", resultaten);
                rd = request.getRequestDispatcher("resultatenWijzigen.jsp");
            } else if (request.getParameter("wijzigResultaat") != null) {
                Long id = Long.parseLong(request.getParameter("wijzigResultaat"));
                float score = Float.parseFloat(request.getParameter("resultaat"));

                client.target(base_url + "resultaten/edit/" + id + "/" + score).request(MediaType.APPLICATION_XML).get(new GenericType<Resultaat>() {
                });

                resultatenWijzigenOpvullen(request);
                rd = request.getRequestDispatcher("resultatenWijzigen.jsp");
            } else {
                rd = request.getRequestDispatcher("index.jsp");
            }

        } else {
            request.setAttribute("boodschap", loginBoodschap);
            rd = request.getRequestDispatcher("login.jsp");
        }

        rd.forward(request, response);

    }

    private RequestDispatcher toonPaginaRapportenDownloaden(HttpServletRequest request) {
        RequestDispatcher rd;
        List<Test> testen = getApiTest("getAll");
        request.setAttribute("testen", testen);
        List<Student> studenten = getApiStudent("getAll");
        request.setAttribute("studenten", studenten);
        rd = request.getRequestDispatcher("rapportDownloaden.jsp");
        return rd;
    }

    public List<Vak> getApiVak(String method) {
        return client.target(base_url + "vakken/" + method).request(MediaType.APPLICATION_XML).get(new GenericType<List<Vak>>() {
        });
    }

    public List<Klas> getApiKlas(String method) {
        return client.target(base_url + "klassen/" + method).request(MediaType.APPLICATION_XML).get(new GenericType<List<Klas>>() {
        });
    }

    public List<Student> getApiStudent(String method) {
        return client.target(base_url + "studenten/" + method).request(MediaType.APPLICATION_XML).get(new GenericType<List<Student>>() {
        });
    }

    public List<Test> getApiTest(String method) {
        return client.target(base_url + "testen/" + method).request(MediaType.APPLICATION_XML).get(new GenericType<List<Test>>() {
        });
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ManageServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ManageServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void resultatenWijzigenOpvullen(HttpServletRequest request) {
        List<Test> testen = getApiTest("getAll");
        request.setAttribute("testen", testen);

        List<Student> studenten = getApiStudent("getAll");
        request.setAttribute("studenten", studenten);
    }

}
