/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Hendrik
 */
@NamedQueries({
    @NamedQuery(name = "Resultaat.getAll", query = "select r from Resultaat r"),
    @NamedQuery(name = "Resultaat.getAllByTest", query = "select r from Resultaat r where r.test.id = :testId")
})

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Resultaat implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private float behaaldCijfer;

    @ManyToOne
    @XmlElement
    private Student student;

    @ManyToOne
    @XmlElement
    private Test test;

    public Resultaat() {
    }

    public Resultaat(Student student, Test test, float behaaldCijfer) {
        this.behaaldCijfer = behaaldCijfer;
        this.student = student;
        this.test = test;
    }

    public float getBehaaldCijfer() {
        return behaaldCijfer;
    }

    public void setBehaaldCijfer(float behaaldCijfer) {
        this.behaaldCijfer = behaaldCijfer;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resultaat)) {
            return false;
        }
        Resultaat other = (Resultaat) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.toegepaste.www.entity.Resultaat[ id=" + id + " ]";
    }

}
