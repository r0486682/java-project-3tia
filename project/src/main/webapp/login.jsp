<%-- 
        Thomas More opleiding Toegepaste Informatica
        Document   : index
        Created on : 17-okt-2016, 10:32:11
        Author     : Hendrik Clercx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ScoreTracker</title>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/custom.css"/>
        <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">ScoreTracker</a>
                </div>
            </div>
            <div><img src="img/cover.jpg" alt="cover afbeelding" width="100%"/></div>
        </nav>

        <main class="container">
            <div class="col-lg-6 col-lg-offset-3"> 
                <form action="ManageServlet" method="post">
                    <h1>ScoreTracker</h1>
                    <p class="small">
                        <i>E-mailadres: admin@gmail.com</i>
                        <br/>
                        <i>wachtwoord: admin</i>
                    </p>
                    <p>Login met uw e-mailadres en wachtwoord om toegang te verkrijgen tot ScoreTracker.</p>
                    <p class="errorMessage">${boodschap}</p>
                    <div class="form-group">
                        <p><label for="1">E-mailadres: </label><input id="1" class="form-control input-sm" type="email" name="email" placeholder="E-mailadres"/></p>
                    </div>
                    <div class="form-group">
                        <p><label for="2">Wachtwoord: </label><input id="2" class="form-control input-sm" type="password" name="wachtwoord" placeholder="Wachtwoord"/></p>
                    </div>
                    <p><button class="btn bg-primary" name="inloggen" type="submit">Inloggen</button></p>
                </form>

            </div>

        </main>

        <footer class="footer navbar-fixed-bottom" id="footerDetails">
            <div class="container">
                <p>Project Java - Hendrik Clercx, Jordy Dieltjens en Julie Haesendonckx - <a href="http://thomasmore.be">Thomas More Kempen Geel</a></p>
            </div>
        </footer>
    </body>
</html>

