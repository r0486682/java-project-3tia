<%-- 
        Thomas More opleiding Toegepaste Informatica
        Document   : index
        Created on : 17-okt-2016, 10:32:11
        Author     : Hendrik Clercx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<c:if test="${sessionScope.isIngelogd != true}">
    <% response.sendRedirect("login.jsp");%>
</c:if>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ScoreTracker</title>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/custom.css"/>
        <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">ScoreTracker</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
                        <li><a href="ManageServlet?lijstUploaden">Lijsten uploaden</a></li>
                        <li><a href="ManageServlet?rapportDownloaden">Rapporten downloaden</a></li>
                        <li><a href="ManageServlet?resultatenWijzigen">Resultaten wijzigen<span class="sr-only">(current)</span></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="ManageServlet?uitloggen">Uitloggen</a></li>
                    </ul>
                </div>
            </div>
            <div><img src="img/cover.jpg" alt="cover afbeelding" width="100%"/></div>
        </nav>


        <main class="container">

            <form action="ManageServlet" method="post">
                <h1>ScoreTracker</h1>
                <p>Welkom op de ScoreTracker webapplicatie. ScoreTracker biedt u een aantal handige functionaliteiten voor het administratief beheer van studenten en hun resultaten.</p>

                <h2>Overzicht functionaliteiten</h2>
                <p>Klik op een functionaliteit om deze te gebruiken:</p>
                <ul>
                    <li><a href="ManageServlet?lijstUploaden">Lijsten uploaden: klassen- en scorelijsten</a></li>
                    <li><a href="ManageServlet?rapportDownloaden">Rapporten downloaden</a></li>
                    <li><a href="ManageServlet?resultatenWijzigen"/>Resultaten van studenten wijzigen </a></li>
                </ul>
            </form>
        </main>

        <footer class="footer navbar-fixed-bottom" id="footerDetails">
            <div class="container">
                <p>Project Java - Hendrik Clercx, Jordy Dieltjens en Julie Haesendonckx - <a href="http://thomasmore.be">Thomas More Kempen Geel</a></p>
            </div>
        </footer>
    </body>
</html>

