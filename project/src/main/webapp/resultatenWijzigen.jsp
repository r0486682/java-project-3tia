<%-- 
    Document   : resultatenWijzigen
    Created on : 3-nov-2016, 21:59:41
    Author     : 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<c:if test="${sessionScope.isIngelogd != true}">
    <% response.sendRedirect("login.jsp");%>
</c:if>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ScoreTracker</title>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/custom.css"/>
        <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript">
            function loadGlyphicons() {
                for (let el of document.querySelectorAll('.glyphicon-floppy-saved')) el.style.visibility = 'hidden';
            }

            function invoerControle(clicked_id) {
                var keuzeStudent = document.getElementById("keuzeStudent").value;
                var keuzeTest = document.getElementById("keuzeTest").value;

                if (clicked_id === "toonResultatenStudent" && keuzeStudent == 0) {
                    //toon model
                    $("#boodschap").html("Gelieve eerst een student te kiezen!");
                    $("#modelFout").modal("show");

                    return false;
                } else if (clicked_id === "toonResultatenTest" && keuzeTest == 0) {
                    $("#boodschap").html("Gelieve eerst een test te kiezen!");
                    $("#modelFout").modal("show");

                    return false;
                } else {
                    return true;
                }
            }

            function wijzigInvoervak(klasse) {
                var spanId = "span" + klasse.replace("resultaat", "");
                var waarde = document.getElementById(spanId).innerHTML;

                $('.' + klasse).replaceWith($('<input type="number" size="5" id="nieuw"' + klasse + ' value="' + waarde + '"/>'));

                var floppy = "save" + spanId.replace("span", "");
                var edit = "wijzig" + spanId.replace("span", "");

                document.getElementsByClassName(floppy)[0].style.visibility = 'visible';
                document.getElementsByClassName(edit)[0].style.visibility = 'hidden';

            }

            function wijzigingDoorvoeren(clicked_id) {
                var nieuwResultaat = $("#nieuw").val();
                var maxScore = parseFloat($("#maxScore").html());
                var id = clicked_id.replace("resultaat_", "");

                if ($.isNumeric(nieuwResultaat) && nieuwResultaat >= 0 && nieuwResultaat <= maxScore) {
                    // geldige invoer, dus invoer opslaan
                    window.location.href = "ManageServlet?wijzigResultaat=" + id + "&resultaat=" + nieuwResultaat;
                } else {
                    $("#boodschap").html("Dit is een ongeldig resultaat!");
                    $("#modelFout").modal("show");
                }
            }
        </script>
    </head>
    <body onload="loadGlyphicons()" >
        <nav class="navbar navbar-default">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp">ScoreTracker</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="index.jsp">Home</a></li>
                        <li><a href="ManageServlet?lijstUploaden">Lijsten uploaden</a></li>
                        <li><a href="ManageServlet?rapportDownloaden">Rapporten downloaden</a></li>
                        <li class="active"><a href="#">Resultaten wijzigen<span class="sr-only">(current)</span></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="ManageServlet?uitloggen">Uitloggen</a></li>
                    </ul>
                </div>
            </div>
            <div><img src="img/cover.jpg" alt="cover afbeelding" width="100%"/></div>
        </nav>

        <main class="container">
            <h1>Resultaten wijzigen</h1>
            <p>Op deze pagina kan u de scores van studenten wijzigen.</p>

            <h2>Kies een student</h2>
            <form id="kiesStudent" action="ManageServlet" method="post">
                <p>
                    <select id="keuzeStudent" name="keuzeStudent" class="input-sm">
                        <option value="0" selected disabled>-- maak een keuze --</option>
                        <c:forEach var="student" items="${studenten}">
                            <option value="${student.id}">${student.getVoornaam()} ${student.getAchternaam()}</option>
                        </c:forEach>
                    </select>
                    <input type="submit" class="btn btn-primary btn-sm" id="toonResultatenStudent" name="resultatenStudent" value="Toon resultaten" onclick="return invoerControle(this.id);"/>
                </p>
            </form>

            <h2>Kies een test</h2>
            <form id="kiesTest" action="ManageServlet" method="post">
                <p>
                    <select class="input-sm" id="keuzeTest" name="keuzeTest" >
                        <option value="0" selected disabled>-- maak een keuze --</option>
                        <c:forEach var="test" items="${testen}">
                            <option value="${test.id}">${test.getNaam()}</option>
                        </c:forEach>
                    </select>
                    <input type="submit" class="btn btn-primary btn-sm" id="toonResultatenTest" name="resultatenTest" value="Toon resultaten" onclick="return invoerControle(this.id);"/>
                </p>
            </form>

            <br>
            <br>

            <!-- if (parameter != null -->
            <c:choose>
                <c:when test="${resultaten.size() > 0}">
                    <table class="table table-hover">
                        <tr>
                            <th>Vak</th>
                            <th>Test</th>
                            <th>Student</th>
                            <th>Datum</th>
                            <th>Score</th>
                            <th></th>
                        </tr>        
                        <c:forEach var="resultaat" items="${resultaten}">
                            <tr>
                                <td>${resultaat.test.vak.getNaam()}</td>
                                <td>${resultaat.test.getNaam()}</td>
                                <td>${resultaat.student.getVoornaam()} ${resultaat.student.getAchternaam()}</td>
                                <td><fmt:formatDate pattern="dd-MM-yyyy" value="${resultaat.test.datum.time}" /></td>
                                <td><span class="resultaat${resultaat.id}" id="span${resultaat.id}">${resultaat.getBehaaldCijfer()}</span>/<span id="maxScore">${resultaat.test.maxScore}</span></td>
                                <td>
                                    <span class="glyphicon glyphicon-edit wijzig${resultaat.id}" id="resultaat${resultaat.id}" onclick="wijzigInvoervak(this.id)"></span>
                                    <span class="glyphicon glyphicon-floppy-saved save${resultaat.id}" id="resultaat_${resultaat.id}" onclick="wijzigingDoorvoeren(this.id)"></span>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:when>
                <c:when test="${resultaten.size() == 0}">
                    <p class="errorMessage">Er werden geen gegevens gevonden...</p>
                </c:when>
            </c:choose>
        </main>

        <footer class="footer navbar-fixed-bottom" id="footerDetails">
            <div class="container">
                <p>Project Java - Hendrik Clercx, Jordy Dieltjens en Julie Haesendonckx - <a href="http://thomasmore.be">Thomas More Kempen Geel</a></p>
            </div>
        </footer>
    </body>
</html>

<!-- Toont fout melding -->
<div class="modal fade" tabindex="-1" id="modelFout" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="titel" class="modal-title">Oeps...</h4>
            </div>
            <div class="modal-body">
                <p id="boodschap" class="text-center"></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>


