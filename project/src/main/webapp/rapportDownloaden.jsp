<%-- 
        Thomas More opleiding Toegepaste Informatica
        Document   : index
        Created on : 17-okt-2016, 10:32:11
        Author     : Hendrik Clercx
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:if test="${sessionScope.isIngelogd != true}">
    <% response.sendRedirect("login.jsp");%>
</c:if>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ScoreTracker</title>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/custom.css"/>
        <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </head>

    <script type="text/javascript">
        var periodeKeuze = "0";
        var boodschap = "";
        var begindatum = "";
        var einddatum = "";

        $(document).ready(function () {
            $("#trimesterBlock").hide();
            $("#semesterBlock").hide();
            $("#datumsBlock").hide();

            $("#keuzePeriode").change(function () {
                periodeKeuze = $(this).val().toString();

                toonJuistePeriodeBlok(periodeKeuze);
            });


            $("#formTestDownloaden").submit(function (e) {
                e.preventDefault();
                var id = $("#keuzeTest").find("option:selected").val();

                if (id != 0) {
                    window.location.href = "../rest-project/rest/testen/downloadRapport/" + id;
                    $("#titel").html("Succes");
                    boodschap = "Rapport is succesvol gedownload!";
                } else {
                    $("#titel").html("Oeps...");
                    boodschap = "Gelieve eerst een test te kiezen!";
                }

                //toon model
                toonErrorModel();

            });

            $("#formPerodiekRaport").submit(function (e) {
                e.preventDefault();

                var isOk = validatePerodiekRapport();

                if (isOk) {
                    var studentId = $("#keuzeStudent").val();
                    var keuzePeriode = $("#keuzePeriode").val();

                    //vult begindatum en einddatum in
                    bepaalDatums();

                    window.location.href = "../rest-project/rest/testen/downloadPerodiekRapport/" + studentId + "/" + begindatum + "/" + einddatum;

                    $("#titel").html("Succes");
                    boodschap = "Rapport is succesvol gedownload!";
                } else {
                    $("#titel").html("Oeps...");
                    //boodschap wordt opgevuld in validatePerodiekRapport()
                }

                toonErrorModel();

            });
        });

        function bepaalDatums() {
            var periode = $("#keuzePeriode").val();
            var currentYear = (new Date).getFullYear();

            if (periode == "trimester") {
                var trim = $("#trimesterBlock").find("input:checked").val();

                switch (trim) {
                    case "trim1":
                        begindatum = currentYear + "-9-1";
                        einddatum = currentYear + "-12-31";
                        break;
                    case "trim2":
                        begindatum = currentYear + "-1-1";
                        einddatum = currentYear + "-3-31";
                        break;
                    case "trim3":
                        begindatum = currentYear + "-4-1";
                        einddatum = currentYear + "-7-30";
                        break;
                }
            } else if (periode == "semester") {
                var sem = $("#semesterBlock").find("input:checked").val();

                switch (sem) {
                    case "sem1":
                        begindatum = currentYear + "-9-1";
                        einddatum = currentYear + "-1-31";
                        break;
                    case "sem2":
                        begindatum = currentYear + "-2-1";
                        einddatum = currentYear + "-7-30";
                        break;
                }


            } else if (periode == "datums") {
                begindatum = $("#begindatum").val();
                einddatum = $("#einddatum").val();
            }
        }

        function toonJuistePeriodeBlok(keuze) {
            verbergVorigeBlocks();
            uncheckVorigeRadioButtons();

            if (keuze === "trimester") {
                $("#trimesterBlock").slideToggle(500);
            } else if (keuze === "semester") {
                $("#semesterBlock").slideToggle(500);
            } else if (keuze === "datums") {
                $("#datumsBlock").slideToggle(500);
            }
        }

        function verbergVorigeBlocks() {
            if ($("#trimesterBlock").is(":visible")) {
                $("#trimesterBlock").slideUp(500);
            }
            if ($("#semesterBlock").is(":visible")) {
                $("#semesterBlock").slideUp(500);
            }
            if ($("#datumsBlock").is(":visible")) {
                $("#datumsBlock").slideUp(500);
            }
        }

        function validatePerodiekRapport() {
            var isOk = true;
            var id = $("#keuzeStudent").find("option:selected").val();
            var keuzePeriode = $("#keuzePeriode").find("option:selected").val();

            boodschap = "";

            if (id == 0) {
                boodschap += "U heeft nog geen student gekozen! ";
                isOk = false;
            }
            if (keuzePeriode == 0) {
                boodschap += "U bent vergeten uw periode te kiezen! ";
                isOk = false;
            }


            //nog steeds valide? verder gaan voor periode
            if (isOk) {
                if (keuzePeriode == "trimester") {
                    var trim = $("#trimesterBlock").find("input:checked").val();

                    if (trim == null) {
                        isOk = false;
                    }
                    boodschap += "U heeft nog geen trimester gekozen.";

                } else if (keuzePeriode == "semester") {
                    var sem = $("#semesterBlock").find("input:checked").val();

                    if (sem == null) {
                        isOk = false;
                    }
                    boodschap += "U heeft nog geen semester gekozen.";

                } else if (keuzePeriode == "datums") {
                    var begin = $("#begindatum").val();
                    var eind = $("#einddatum").val();

                    if (begin == "" || eind == "") {
                        isOk = false;
                    }
                    boodschap += "U moet beide datums invullen.";
                }
            }

            return isOk;
        }

        function uncheckVorigeRadioButtons() {
            $("#trimesterBlock, #semesterBlock").find("input:checked").each(function () {
                if ($(this).prop("checked")) {
                    $(this).prop("checked", false);
                }
            });

        }

        function toonErrorModel() {
            $("#boodschap").html(boodschap);
            $("#modelFout").modal("show");
        }

    </script>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp">ScoreTracker</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="index.jsp">Home</a></li>
                        <li ><a href="ManageServlet?lijstUploaden">Lijsten uploaden</a></li>
                        <li class="active"><a href="#">Rapporten downloaden<span class="sr-only">(current)</span></a></li>
			<li><a href="ManageServlet?resultatenWijzigen">Resultaten wijzigen</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="ManageServlet?uitloggen">Uitloggen</a></li>
                    </ul>
                </div>
            </div>
            <div><img src="img/cover.jpg" alt="cover afbeelding" width="100%"/></div>
        </nav>

        <main class="container">
            <div class="col-lg-12">
                <form id="formTestDownloaden" action="ManageServlet?testDownloaden" method="post">

                    <h1>Rapporten</h1>
                    <p>Op deze pagina kan u rapporten downloaden in PDF-formaat.</p>
                    <p>U kan kiezen om alle resultaten voor een bepaalde test op te vragen of </p>

                    <h2>Rapport van een test downloaden</h2>
                    <p>kies een test uit de lijst:</p>
                    <p>
                        <select class="input-sm" id="keuzeTest" >
                            <option  value="0">-- maak een keuze --</option>
                            <c:forEach var="test" items="${testen}">
                                <option value="${test.id}">${test.getNaam()}</option>
                            </c:forEach>
                        </select>
                        <button class="btn btn-primary btn-sm" id="knopDownloadTest">Download rapport</button>
                    </p>
                </form>


                <form id="formPerodiekRaport" action="ManageServlet?PeriodiekRapportDownloaden" method="post">
                    <h2>Perodiek rapport downloaden</h2>
                    <p>kies een student uit de lijst:</p>
                    <select id="keuzeStudent" class="input-sm">
                        <option value="0">-- maak een keuze --</option>
                        <c:forEach var="student" items="${studenten}">
                            <option  value="${student.id}">${student.getVoornaam()}  ${student.getAchternaam()}</option>
                        </c:forEach>
                    </select>

                    <p></p>

                    <p>Specifier een periode:</p>
                    <p>
                        <select class="input-sm" id="keuzePeriode">
                            <option value="0">-- maak een keuze --</option>
                            <option value="trimester">Trimester</option>
                            <option value="semester">Semester</option>
                            <option value="datums">Tussen 2 datums</option>
                        </select> 
                        <button class="btn btn-primary btn-sm" id="knopPerodiekRapportDownloaden">Download rapport</button></p>
                    </p>


                    <div id="trimesterBlock">
                        <ul>
                            <li><input type="radio" id="tr1" value="trim1"/> <label for="tr1">Trimester 1</label></li>
                            <li><input type="radio" id="tr2" value="trim2"/> <label for="tr2">Trimester 2</label></li>
                            <li><input type="radio" id="tr3" value="trim3"/> <label for="tr3">Trimester 3</label></li>
                        </ul>
                    </div>
                    <div id="semesterBlock">
                        <ul>
                            <li><input type="radio" id="sm1" value="sem1"/> <label for="sm1">Semester 1</label></li>
                            <li><input type="radio" id="sm2" value="sem2"/> <label for="sm2">Semester 2</label></li>
                        </ul>
                    </div>
                    <div id="datumsBlock">
                        <p>Tussen 2 datums</p>
                        <p><input class="input-sm" type="date" id="begindatum"/> - <input class="input-sm" type="date" id="einddatum"/></p>
                    </div>



                </form>
            </div>
        </main>

        <footer class="footer navbar-fixed-bottom" id="footerDetails">
            <div class="container">
                <p>Project Java - Hendrik Clercx, Jordy Dieltjens en Julie Haesendonckx - <a href="http://thomasmore.be">Thomas More Kempen Geel</a></p>
            </div>
        </footer>
    </body>
</html>

<!-- Toont fout melding -->
<div class="modal fade" tabindex="-1" id="modelFout" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="titel" class="modal-title">Oeps...</h4>
            </div>
            <div class="modal-body">
                <p id="boodschap" class="text-center"></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>