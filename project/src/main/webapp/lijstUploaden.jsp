<%-- 
        Thomas More opleiding Toegepaste Informatica
        Document   : index
        Created on : 17-okt-2016, 10:32:11
        Author     : 
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:if test="${sessionScope.isIngelogd != true}">
    <% response.sendRedirect("login.jsp");%>
</c:if>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ScoreTracker</title>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/custom.css"/>
        <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript">
	    $(document).ready(function () {
		var querystring = location.search;
		if (querystring) {
		    // bekijk parameter
		    querystring = querystring.substring(querystring.lastIndexOf('=') + 1);

		    if (querystring === "true") {
			toonErrorModel("Verkeerde formaat van klaslijst. Gelieve de sjabloon te gebruiken van klaslijsten");
		    }else if (querystring === "klasnotfound"){
                        
                        toonErrorModel("Deze klas bestaat al!");
                    }
		}

                $("input:file").change(function(e){
                    
                   var file = $(this).val();
		    if (!valideUpload(file)) {
			
			toonErrorModel("Dit is een ongeldig bestandstype, geldige bestandstypes zijn: .xlsx, .xls en .csv");
                        $(this).val("");
		    }
                });
		
	    });

	    function toonErrorModel(message) {
		$("#boodschap").html(message);
		$("#modelFout").modal("show");
		
	    }

	    function valideUpload(file) {
		var validExts = new Array(".xlsx", ".xls", ".csv");
		var fileExt = file.substring(file.lastIndexOf('.'));
                
		return validExts.indexOf(fileExt) !== -1;
	    }

	    

        </script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp">ScoreTracker</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="index.jsp">Home</a></li>
                        <li class="active"><a href="#">Lijsten uploaden<span class="sr-only">(current)</span></a></li>
                        <li><a href="ManageServlet?rapportDownloaden">Rapporten downloaden</a></li>
			<li><a href="ManageServlet?resultatenWijzigen">Resultaten wijzigen</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="ManageServlet?uitloggen">Uitloggen</a></li>
                    </ul>
                </div>
            </div>
            <div><img src="img/cover.jpg" alt="cover afbeelding" width="100%"/></div>
        </nav>
        <main class="container">
            <form id="formKlaslijst" action="../rest-project/rest/studenten/uploadStudenten" method="post" enctype='multipart/form-data'>
                <h1>Lijst uploaden <c:out value="${klas}" /></h1>
                <p>Op deze pagina kan u een klassenlijst of een scorelijst uploaden.</p>

                <h2>Klaslijst uploaden</h2>
		<p>Download template: <a href="templates/sjabloon_klaslijst.xlsx">klik hier</a></p>
                <p>Vul een klasnaam in:</p>
		<p>
		    <label for="klas" class="control-label" >Klas</label>
                    <input type="text" class="input-sm" id="klas" name="klas" placeholder="klas" required/>
		</p>
                <p>Upload een klaslijst <input type="file" name="klaslijst" id="klaslijst" class="fileupload" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required/></p>
                <p><button class="btn btn-primary btn-sm" name="knopKlasToevoegen" id="knopKlasUploaden">Klas toevoegen</button></p>
            </form>


            <form id="formScorelijst" action="../rest-project/rest/resultaten/uploaden" method="POST" enctype="multipart/form-data">
                <h2>Scorelijst uploaden</h2>
                <p>Download template: <a href="templates/sjabloon_puntenlijst.xlsx">klik hier</a></p>
                <p>Upload een Excelbestand</p>
		<p><input type="file" name="file" id="scorelijst" class="fileupload" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required /></p>
                <p><button class="btn btn-primary btn-sm" name="scorelijstUploaden" id="knopScorelijstUploaden">Scorelijst toevoegen</button></p>
            </form>
        </main>
        <footer class="footer navbar-fixed-bottom" id="footerDetails">
            <div class="container">
                <p>Project Java - Hendrik Clercx, Jordy Dieltjens en Julie Haesendonckx - <a href="http://thomasmore.be">Thomas More Kempen Geel</a></p>
            </div>
        </footer>
    </body>
</html>

<!-- Toont fout melding -->
<div class="modal fade" tabindex="-1" id="modelFout" role="dialog">
    <div class="modal-dialog modal-sm">
	<div class="modal-content">
	    <div class="modal-header">
		<button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 id="titel" class="modal-title">Oeps...</h4>
	    </div>
	    <div class="modal-body">
		<p id="boodschap" class="text-center"></p>
	    </div>
	    <div class="modal-footer">
		<button class="btn btn-primary" data-dismiss="modal">OK</button>
	    </div>
	</div>
    </div>
</div>