<%-- 
    Document   : testresultaten
    Created on : 1-nov-2016, 20:50:39
    Author     : Julie
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<c:if test="${sessionScope.isIngelogd != true}">
    <% response.sendRedirect("login.jsp");%>
</c:if>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ScoreTracker</title>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/custom.css"/>
        <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">LOGO</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.jsp">Home</a></li>
                        <li><a href="ManageServlet?lijstUploaden">Lijsten uploaden</a></li>
                        <li><a href="ManageServlet?rapportDownloaden">Rapporten downloaden</a></li>
                        <li><a href="ManageServlet?resultatenWijzigen">Resultaten wijzigen</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="ManageServlet?uitloggen">Uitloggen</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="container">
            <h1>Resultaten</h1>
            <p>Dit zijn de resultaten voor de test ${test.naam} van het vak  ${test.vak.naam} afgelegd op: <fmt:formatDate pattern="dd-MM-yyyy" value="${test.datum.time}" /></p>

            <table class="table table-striped">
                <tr>
                    <th>Naam student</th>
                    <th>Voornaam student</th>
                    <th>Score student</th>
                </tr>
                <c:forEach var="resultaat"  items="${resultaten}">
                    <tr>
                        <td><c:out value="${resultaat.student.achternaam}" /></td>
                        <td><c:out value="${resultaat.student.voornaam}" /></td>
                        <td><c:out value="${resultaat.behaaldCijfer}/${test.maxScore}" /></td>
                    </tr>
                </c:forEach>
            </table>
            <a href="ManageServlet?lijstUploaden"><button class="btn btn-primary btn-sm">Terug</button></a>
        </main>

        <footer class="footer navbar-fixed-bottom" id="footerDetails">
            <div class="container">
                <p>Project Java - Hendrik Clercx, Jordy Dieltjens en Julie Haesendonckx - <a href="http://thomasmore.be">Thomas More Kempen Geel</a></p>
            </div>
        </footer>
    </body>
</html>
