<%-- 
    Document   : overzicht
    Created on : Oct 10, 2016, 9:46:59 AM
    Author     : Jordy Dieltjens
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>

<c:if test="${sessionScope.isIngelogd != true}">
    <% response.sendRedirect("login.jsp");%>
</c:if>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ScoreTracker</title>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/custom.css"/>
        <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">ScoreTracker</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="index.jsp">Home <span class="sr-only">(current)</span></a></li>
                        <li class="active"><a href="ManageServlet?lijstUploaden">Lijsten uploaden</a></li>
                        <li><a href="ManageServlet?rapportDownloaden">Rapporten downloaden</a></li>
                        <li><a href="ManageServlet?resultatenWijzigen">Resultaten wijzigen</a></li>

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="ManageServlet?uitloggen">Uitloggen</a></li>
                    </ul>
                </div>
            </div>
            <div><img src="img/cover.jpg" alt="cover afbeelding" width="100%"/></div>
        </nav>

        <main class="container">
            <h1>Overzicht van de studenten van de klas <c:out value="${klas}" /></h1>

            <table class="table .table-hover">
                <thead>
                    <tr>
                        <th>Voornaam</th>
                        <th>Achternaam</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="student"  items="${studenten}">
                        <tr>
                            <td><c:out value="${student.voornaam}" /></td><td> <c:out value="${student.achternaam}" /> </td><td> <c:out value="${student.email}" /></td>
                        </tr>
                    </c:forEach>
                <tbody>

            </table>
        </main>

        <footer class="footer navbar-fixed-bottom" id="footerDetails">
            <div class="container">
                <p>Project Java - Hendrik Clercx, Jordy Dieltjens en Julie Haesendonckx - <a href="http://thomasmore.be">Thomas More Kempen Geel</a></p>
            </div>
        </footer>
    </body>
</html>