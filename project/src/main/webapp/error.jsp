<%-- 
    
    Thomas More opleiding Toegepaste Informatica - Fase 1
    Document   : error
    Created on : 4-nov-2016, 14:24:03
    Author     : Julie Haesendonckx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:if test="${sessionScope.isIngelogd != true}">
    <% response.sendRedirect("login.jsp");%>
</c:if>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ScoreTracker</title>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/custom.css"/>
        <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp">ScoreTracker</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="index.jsp">Home</a></li>
                        <li><a href="ManageServlet?lijstUploaden">Lijsten uploaden</a></li>
                        <li><a href="ManageServlet?rapportDownloaden">Rapporten downloaden</a></li>
                        <li><a href="ManageServlet?resultatenWijzigen"></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="ManageServlet?uitloggen">Uitloggen</a></li>
                    </ul>
                </div>
            </div>
            <div><img src="img/cover.jpg" alt="cover afbeelding" width="100%"/></div>
        </nav>

        <main class="container">
            <h1>Error!</h1>
            <div class="col-md-5"><img src="img/error.png" alt="error"/></div>
            <div class="col-md-7">
                <p>Er is een fout opgetreden.</p>
                <p>Sorry voor het ongemak!</p>
                <a href="index.jsp">Ga terug naar de homepage</a>
            </div>            
        </main>

        <footer class="footer navbar-fixed-bottom" id="footerDetails">
            <div class="container">
                <p>Project Java - Hendrik Clercx, Jordy Dieltjens en Julie Haesendonckx - <a href="http://thomasmore.be">Thomas More Kempen Geel</a></p>
            </div>
        </footer>
    </body>
</html>