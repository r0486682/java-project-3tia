/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.informatica.opdracht1.team3;

import java.io.File;
import java.util.List;

/**
 *
 * @author Hendrik
 */
public interface ExcelReader {

    public List<Student> readStudents(File inputXslxFile);
}
