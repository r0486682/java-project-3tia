/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.informatica.opdracht1.team3;

/**
 *
 * @author Hendrik
 */
public class Student {

    public String lastname, firstname, email;

    public Student() {

    }

    public Student(String lastname, String firstname, String email) {
	this.lastname = lastname;
	this.firstname = firstname;
	this.email = email;
    }

    public String getLastname() {
	return lastname;
    }

    public void setLastname(String lastname) {
	this.lastname = lastname;
    }

    public String getFirstname() {
	return firstname;
    }

    public void setFirstname(String firstname) {
	this.firstname = firstname;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

}
