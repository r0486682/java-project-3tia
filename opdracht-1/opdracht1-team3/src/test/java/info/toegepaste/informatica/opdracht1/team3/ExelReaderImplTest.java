/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.informatica.opdracht1.team3;

import java.io.File;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author Hendrik
 */
public class ExelReaderImplTest extends TestCase {

    public ExelReaderImplTest(String testName) {
	super(testName);
    }

    /*
    @Override
    protected void setUp() throws Exception {
	super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
	super.tearDown();
    }
     */
    
    /**
     * Test of readStudents method, of class ExelReaderImpl.
     */
    public void testReadStudents() {
	System.out.println("--- Studenten lezen ---");

	String path = Thread.currentThread().getContextClassLoader().getResource("testexcel.xlsx").getPath();
	File inputXslxFile = new File(path);

	ExelReaderImpl instance = new ExelReaderImpl();

	List<Student> studenten = instance.readStudents(inputXslxFile);

	// controleren of het object studeten gevuld wordt met de gegevens van het .xlsx bestand
	assertNotNull(studenten);
	assertEquals(2, studenten.size());
	assertEquals("Clinton", studenten.get(0).lastname);
    }

}
