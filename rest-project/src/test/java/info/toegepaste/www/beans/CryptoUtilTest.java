/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.beans;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hendrik
 */
public class CryptoUtilTest {

    public CryptoUtilTest() {
    }

    /**
     * Test of encrypt method, of class CryptoUtil.
     */
    @Test
    public void testEncrypt() throws Exception {
	System.out.println("encrypt");
	String secretKey = "test";
	String plainText = "tekst";
	CryptoUtil instance = new CryptoUtil();

	String result = instance.encrypt(secretKey, plainText);

	assertEquals(plainText, instance.decrypt(secretKey, result));

    }

    /**
     * Test of decrypt method, of class CryptoUtil.
     */
    @Test
    public void testDecrypt() throws Exception {
	System.out.println("decrypt");
	String secretKey = "test";
	String encryptedText = "WCcRsn5JgAE=";
	CryptoUtil instance = new CryptoUtil();

	String decryptedText = instance.decrypt(secretKey, encryptedText);

	assertEquals(instance.encrypt(secretKey, decryptedText), encryptedText);
    }

}
