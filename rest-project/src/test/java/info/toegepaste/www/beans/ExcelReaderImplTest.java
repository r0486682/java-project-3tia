/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.beans;

import info.toegepaste.www.entity.Klas;
import info.toegepaste.www.entity.Student;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import static junit.framework.TestCase.assertNotNull;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordy Dieltjens
 */
public class ExcelReaderImplTest {

    public ExcelReaderImplTest() {
    }

    /**
     * Test of readStudents method, of class ExcelReaderImpl.
     */
    @Test
    public void testReadStudents() throws FileNotFoundException {
        System.out.println("readStudents");
        InputStream file = null;
        String path = Thread.currentThread().getContextClassLoader().getResource("testexcelKlas.xlsx").getPath();

        file = new FileInputStream(new File(path));
        ExcelReaderImpl instance = new ExcelReaderImpl();

        Klas klas = new Klas("test");

        List<Student> studenten = new ArrayList();

        Student student1 = new Student("Dieltjens", "Jordy", "jordy@hotmail.com", klas);
        Student student2 = new Student("Clerlcx", "Hendrik", "henny@hotmail.com", klas);
        Student student3 = new Student("Haesendonxc", "Julie", "julie@hotmail.com", klas);

        studenten.add(student1);
        studenten.add(student2);
        studenten.add(student3);

        List<Student> result = instance.readStudents(file);
        assertNotNull(studenten);
        assertEquals(studenten.size(), result.size());
        assertEquals(studenten, result);
        // TODO review the generated test code and remove the default call to fail.

    }

}
