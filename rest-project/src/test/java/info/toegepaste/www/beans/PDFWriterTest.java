/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.beans;

import info.toegepaste.www.entity.Klas;
import info.toegepaste.www.entity.Student;
import info.toegepaste.www.entity.Vak;
import java.io.OutputStream;
import java.util.GregorianCalendar;
import org.junit.Test;

/**
 *
 * @author Hendrik
 */
public class PDFWriterTest {

    public PDFWriterTest() {
    }

    /**
     * Test of createTestReport method, of class PDFWriter.
     */
    @Test
    public void testCreateTestReport() {
	System.out.println("createTestReport");
	OutputStream outputStream = null;
	GregorianCalendar cal = new GregorianCalendar();
	cal.set(2016, 11, 6);
	info.toegepaste.www.entity.Test test = new info.toegepaste.www.entity.Test("SpellingsTest", 20, new Vak("Nederlands"), cal);

	PDFWriter pdfWriter = new PDFWriterImpl();
	pdfWriter.createTestReport(outputStream, test);
    }

    /**
     * Test of createPeriodiekReport method, of class PDFWriterImpl.
     */
    @Test
    public void testCreatePeriodiekReport() {
	System.out.println("createPeriodiekReport");
	OutputStream outputStream = null;
	GregorianCalendar datummTest = new GregorianCalendar();
	datummTest.set(2016, 11, 6);
	GregorianCalendar begindatum = new GregorianCalendar();
	begindatum.set(2015, 11, 6);
	GregorianCalendar einddatum = new GregorianCalendar();
	einddatum.set(2016, 5, 14);

	PDFWriter pdfWriter = new PDFWriterImpl();
	pdfWriter.createPeriodiekReport(outputStream, new Student("janssens", "Pieter", "pieter-janssens@gmail.com", new Klas("2TI")), begindatum, einddatum, "Tussen 2 datums");
    }

    public class PDFWriterImpl implements PDFWriter {

	public void createTestReport(OutputStream outputStream, info.toegepaste.www.entity.Test test) {
	}

	public void createPeriodiekReport(OutputStream outputStream, Student student, GregorianCalendar begindatum, GregorianCalendar einddatum, String periode) {
	}
    }

}
