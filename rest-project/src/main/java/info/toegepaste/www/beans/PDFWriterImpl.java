package info.toegepaste.www.beans;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.DeviceRgb;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;

import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import info.toegepaste.www.entity.Student;
import info.toegepaste.www.entity.Test;
import java.io.IOException;
import java.io.OutputStream;
import com.itextpdf.layout.element.Table;
import info.toegepaste.www.ejb.ResultaatService;
import info.toegepaste.www.entity.Resultaat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class PDFWriterImpl implements PDFWriter {

    ResultaatService resultaatService = lookupResultaatServiceBean();

    PdfFont font;
    PdfFont fontBold;
    SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE dd/MM/yyyy");

    public PDFWriterImpl() throws IOException {
	this.font = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
	this.fontBold = PdfFontFactory.createFont(FontConstants.TIMES_BOLD);
    }

    @Override
    public void createTestReport(OutputStream outputStream, Test test) {
	// Nieuw document
	PdfDocument pdfDocument = new PdfDocument(new PdfWriter(outputStream));
	Document doc = new Document(pdfDocument);

	//titel
	doc.add(new Paragraph("Rapport").setFontSize(24).setFont(fontBold).setFontColor(new DeviceRgb(68, 125, 168)));

	//subtitel
	doc.add(new Paragraph("Test: " + test.getNaam()).setFont(font));
	doc.add(new Paragraph("Vak: " + test.getVak().getNaam()).setFont(font));
	doc.add(new Paragraph("Datum van test: " + dateFormat.format(test.getDatum().getTime())).setFont(font));

	//tabel met gegevens over de test
	Table tabel = new Table(4);

	//header
	tabel.addHeaderCell(new Cell().add("#").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));
	tabel.addHeaderCell(new Cell().add("Student").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));
	tabel.addHeaderCell(new Cell().add("Behaald cijfer").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));
	tabel.addHeaderCell(new Cell().add("Maximumscore").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));

	//content
	int teller = 1;
	if (test.getResultaten().size() > 0) {
	    for (Resultaat r : test.getResultaten()) {
		tabel.addCell(teller + "");
		tabel.addCell(r.getStudent().getVoornaam() + " " + r.getStudent().getAchternaam()).setFont(font);
		tabel.addCell(r.getBehaaldCijfer() + "").setFont(font);
		tabel.addCell(test.getMaxScore() + "").setFont(font);

		teller++;
	    }

	    tabel.setMarginBottom(25f);
	    doc.add(tabel);

	    //Tabel met gemiddelde en de mediaan
	    Table tabelExtraInfo = new Table(2);
	    tabelExtraInfo.setWidthPercent(50f);
	    tabelExtraInfo.addHeaderCell(new Cell().add("Mediaan").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));
	    tabelExtraInfo.addHeaderCell(new Cell().add("Gemiddelde").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));
	    tabelExtraInfo.addCell(test.getMediaan() + "").setFont(font);
	    tabelExtraInfo.addCell(String.format("%.2f", test.getGemiddelde())).setFont(font);
	    doc.add(tabelExtraInfo);
	} else {
	    doc.add(new Paragraph("Geen resultaten gevonden voor deze test...")).setFont(font);
	}

	// Document afsluiten
	doc.close();
    }

    @Override
    public void createPeriodiekReport(OutputStream outputStream, Student student, GregorianCalendar begindatum, GregorianCalendar einddatum, String periode) {
	//nieuw doc
	PdfDocument pdfDocument = new PdfDocument(new PdfWriter(outputStream));
	Document doc = new Document(pdfDocument);

	//titel
	doc.add(new Paragraph("Rapport: " + periode).setFontSize(24).setFont(fontBold).setFontColor(new DeviceRgb(68, 125, 168)));
	doc.add(new Paragraph("Periode: " + dateFormat.format(begindatum.getTime()) + " - " + dateFormat.format(einddatum.getTime()))).setFont(font);

	doc.add(new Paragraph("Student: " + student.getVoornaam() + " " + student.getAchternaam()).setFont(font));
	doc.add(new Paragraph("Klas: " + student.getKlas().getNaam()).setFont(font));

	//tabel met gegevens over de test
	Table tabel = new Table(5);
	//header
	tabel.addHeaderCell(new Cell().add("#").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));
	tabel.addHeaderCell(new Cell().add("Vak").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));
	tabel.addHeaderCell(new Cell().add("Behaald cijfer").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));
	tabel.addHeaderCell(new Cell().add("Klas gemiddelde").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));
	tabel.addHeaderCell(new Cell().add("Mediaan").setBackgroundColor(new DeviceRgb(125, 169, 202)).setFont(font));

	List<Resultaat> resultaten = resultaatService.getAllFromStudentBetweenDates(student.getId(), begindatum, einddatum);

	//content
	if (resultaten.size() > 0) {
	    int teller = 1;
	    for (Resultaat r : resultaten) {

		tabel.addCell(teller + "");
		tabel.addCell(r.getTest().getVak().getNaam()).setFont(font);
		tabel.addCell(formatScore(procentueleScore(r.getBehaaldCijfer(), r.getTest().getMaxScore()))).setFont(font);
		tabel.addCell(formatScore(procentueleScore(r.getTest().getVak().getGemiddelde(), r.getTest().getMaxScore()))).setFont(font);
		tabel.addCell(formatScore(procentueleScore(r.getTest().getVak().getMediaan(), r.getTest().getMaxScore()))).setFont(font);

		teller++;
	    }

	    tabel.setMarginBottom(25f);
	    doc.add(tabel);
	} else {
	    doc.add(new Paragraph("Geen resultaten gevonden voor de opgegeven periode...")).setFont(font);
	}
	doc.close();
    }

    public String formatScore(float getal) {
	return String.format("%.2f", getal) + "%";
    }

    public float procentueleScore(float score, float maxScore) {
	return score / maxScore * 100;
    }

    private ResultaatService lookupResultaatServiceBean() {
	try {
	    Context c = new InitialContext();
	    return (ResultaatService) c.lookup("java:global/rest-project/ResultaatService!info.toegepaste.www.ejb.ResultaatService");
	} catch (NamingException ne) {
	    Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
	    throw new RuntimeException(ne);
	}
    }

}
