/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.beans;

import info.toegepaste.www.entity.Student;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Jordy
 */
public class ExcelReaderImpl implements ExcelReader {

    Student student;

    @Override
    public List<Student> readStudents(InputStream file) {

        List<Student> studenten = new ArrayList<>();

        try {

            //Get the workbook instance for XLS file 
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int teller = 0;

            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (teller == 0) {
                    if (!row.getCell(0).toString().equals("Naam")) {
                        studenten = null;
                        break;
                    } else if (!row.getCell(1).toString().equals("Voornaam")) {
                        studenten = null;
                        break;
                    } else if (!row.getCell(2).toString().equals("Email")) {
                        studenten = null;
                        break;
                    }
                }
                if (teller > 0) {
                    Student student = new Student();

                    student.setAchternaam(row.getCell(0).toString());
                    student.setVoornaam(row.getCell(1).toString());
                    student.setEmail(row.getCell(2).toString());

                    studenten.add(student);
                }
                teller++;
            }
            file.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return studenten;
    }

}
