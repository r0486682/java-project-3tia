package info.toegepaste.www.beans;

import info.toegepaste.www.entity.Student;
import info.toegepaste.www.entity.Test;
import java.io.OutputStream;
import java.util.GregorianCalendar;

public interface PDFWriter {

    void createTestReport(OutputStream outputStream, Test test);

    void createPeriodiekReport(OutputStream outputStream, Student student, GregorianCalendar begindatum, GregorianCalendar einddatum, String periode);
}
