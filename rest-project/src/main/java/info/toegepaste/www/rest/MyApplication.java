package info.toegepaste.www.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.moxy.xml.MoxyXmlFeature;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("rest")
public class MyApplication extends ResourceConfig {

    public MyApplication() {
        super(MultiPartFeature.class);
        register(MoxyXmlFeature.class);
        packages("info.toegepaste.www.ejb");
        packages("info.toegepaste.www.entity");
    }
}
