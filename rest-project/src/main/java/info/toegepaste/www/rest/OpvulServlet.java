/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.rest;

import info.toegepaste.www.entity.Gebruiker;
import info.toegepaste.www.entity.Klas;
import info.toegepaste.www.entity.Resultaat;
import info.toegepaste.www.entity.Student;
import info.toegepaste.www.entity.Test;
import info.toegepaste.www.entity.Vak;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

/**
 *
 * @author Jordy Dieltjens
 */
@WebServlet(name = "OpvulServlet", urlPatterns = {"/OpvulServlet"})
@Transactional
public class OpvulServlet extends HttpServlet {

    @PersistenceContext(unitName = "ScoreTrackerPU")
    private EntityManager em;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Gebruiker gebruiker = new Gebruiker("admin@gmail.com", "admin");

        //klassen maken
        Klas klas = new Klas("3TiA");
        Klas klas2 = new Klas("3TiB");
        Klas klas3 = new Klas("3TiN");

        //studenten maken
        Student jordy = new Student("Dieltjens", "Jordy", "jordy@hotmail.com", klas);
        Student hendrik = new Student("Clercx", "Hendrik", "hendrik@hotmail.com", klas);
        Student julie = new Student("Haesendoncks", "Julie", "julie@hotmail.com", klas);
        Student matthias = new Student("Claes", "Matthias", "matttC@hotmail.com", klas);
        Student gunther = new Student("Meert", "Gunther", "gunther@gmail.com", klas2);
        Student sven = new Student("Swennen", "Sven", "svenboy96@gmail.com", klas2);
        Student nick = new Student("Vervecken", "Josef", "josefmaria@gmail.com", klas2);
        Student thomas = new Student("Slegers", "Emma", "emmaSlegers@hotmail.com", klas2);
        Student ben = new Student("Leynen", "Ben", "knoebst@gmail.com", klas3);
        Student jef = new Student("Jansssens", "Jef", "jj@hotmail.com", klas3);
        Student pieter = new Student("Luyts", "Pieter", "luytsPieter@hotmail.com", klas3);
        Student tim = new Student("Rover", "Tim De Roover", "timmy@gmail.com", klas3);

        //studenten aan List toevoegen
        List<Student> studentsA = new ArrayList<>();
        studentsA.add(jordy);
        studentsA.add(hendrik);
        studentsA.add(julie);
        studentsA.add(matthias);

        List<Student> studentsB = new ArrayList<>();
        studentsB.add(gunther);
        studentsB.add(sven);
        studentsB.add(nick);
        studentsB.add(thomas);

        List<Student> studentsN = new ArrayList<>();
        studentsN.add(ben);
        studentsN.add(jef);
        studentsN.add(pieter);
        studentsN.add(tim);

        for (Student s : studentsA) {
            s.setKlas(klas);
        }
        for (Student s : studentsB) {
            s.setKlas(klas2);
        }
        for (Student s : studentsN) {
            s.setKlas(klas3);
        }

        //studenten aan klas toewijzen
        klas.setStudenten(studentsA);
        klas2.setStudenten(studentsB);
        klas3.setStudenten(studentsN);

        //vakken maken
        Vak engels = new Vak("Engels");
        Vak frans = new Vak("Frans");
        Vak java = new Vak("Java");
        Vak cSharp = new Vak("C#");
        Vak php = new Vak("PHP");
        Vak linux = new Vak("Linux");

        //testen maken
        Test t1 = new Test("English Writing", 20, engels, new GregorianCalendar(2016, 10, 20));
        Test t2 = new Test("Linux Essentials", 20, linux, new GregorianCalendar(2016, 10, 22));
        Test t3 = new Test("Design patterns", 20, java, new GregorianCalendar(2016, 10, 23));
        Test t4 = new Test("Webproject", 20, php, new GregorianCalendar(2016, 10, 27));
        Test t5 = new Test("English Oral", 20, engels, new GregorianCalendar(2016, 5, 5));

        //resultaten maken
        Resultaat r1 = new Resultaat(sven, t1, 14);
        Resultaat r2 = new Resultaat(jef, t1, 14.5f);
        Resultaat r3 = new Resultaat(hendrik, t1, 17);
        Resultaat r4 = new Resultaat(nick, t2, 13);
        Resultaat r5 = new Resultaat(jordy, t2, 16);
        Resultaat r6 = new Resultaat(julie, t3, 16);
        Resultaat r7 = new Resultaat(gunther, t3, 12.5f);
        Resultaat r8 = new Resultaat(ben, t4, 10.5f);
        Resultaat r9 = new Resultaat(matthias, t4, 11.5f);
        Resultaat r10 = new Resultaat(tim, t4, 11);
        Resultaat r11 = new Resultaat(hendrik, t5, 11);
        Resultaat r12 = new Resultaat(jordy, t5, 11.5f);
        Resultaat r13 = new Resultaat(julie, t5, 13);

        //testen aan juiste vakken toevoegen
        List<Test> testenEngels = new ArrayList<>();
        testenEngels.add(t1);
        engels.setTesten(testenEngels);

        List<Test> testenLinux = new ArrayList<>();
        testenLinux.add(t2);
        linux.setTesten(testenLinux);

        List<Test> testenJava = new ArrayList<>();
        testenJava.add(t3);
        java.setTesten(testenJava);

        List<Test> testenPHP = new ArrayList<>();
        testenPHP.add(t4);
        php.setTesten(testenPHP);

        List<Test> oralExamEngels = new ArrayList<>();
        oralExamEngels.add(t5);
        engels.setTesten(oralExamEngels);

        //resultaten aan test toevoegen
        List<Resultaat> resultatenEngels = new ArrayList<>();
        resultatenEngels.add(r1);
        resultatenEngels.add(r2);
        resultatenEngels.add(r3);
        t1.setResultaten(resultatenEngels);

        List<Resultaat> resultatenLinux = new ArrayList<>();
        resultatenLinux.add(r4);
        resultatenLinux.add(r5);
        t2.setResultaten(resultatenLinux);

        List<Resultaat> resultatenJava = new ArrayList<>();
        resultatenJava.add(r6);
        resultatenJava.add(r7);
        t3.setResultaten(resultatenJava);

        List<Resultaat> resultatenPHP = new ArrayList<>();
        resultatenPHP.add(r8);
        resultatenPHP.add(r9);
        resultatenPHP.add(r10);
        t4.setResultaten(resultatenPHP);

        /*
	PERSIST MAKEN
         */
        //gebruiker
        em.persist(gebruiker);

        //alle studenten in 1 list zetten 
        List<Student> allStudents = new ArrayList<>();
        allStudents.addAll(studentsA);
        allStudents.addAll(studentsB);
        allStudents.addAll(studentsN);

        //studenten persist maken
        for (Student student : allStudents) {
            em.persist(student);
        }

        //klassen persit maken
        em.persist(klas);
        em.persist(klas2);
        em.persist(klas3);

        //alle vakken in 1 list zetten
        List<Vak> allVakken = new ArrayList<>();
        allVakken.add(engels);
        allVakken.add(frans);
        allVakken.add(java);
        allVakken.add(cSharp);
        allVakken.add(php);
        allVakken.add(linux);

        //vakken persist maken
        for (Vak vak : allVakken) {
            em.persist(vak);
        }

        //alle testen in 1 list zetten
        List<Test> allTesten = new ArrayList<>();
        allTesten.add(t1);
        allTesten.add(t2);
        allTesten.add(t3);
        allTesten.add(t4);

        //testen persist maken
        for (Test test : allTesten) {
            em.persist(test);
        }

        System.out.println("-----------------------------------------");
        System.out.println("Objecten zijn toegevoegd aan de database!");
        System.out.println("-----------------------------------------");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
