/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 *
 * @author Hendrik Clercx
 */
@NamedQueries({
    @NamedQuery(name = "Test.getAll", query = "select t from Test t order by t.vak"),
    @NamedQuery(name = "Test.getById", query = "select t from Test t where t.id = :testId")
})

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Test implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private float maxScore;
    private String naam;
    @Temporal(TemporalType.DATE)
    private GregorianCalendar datum;

    @ManyToOne
    @XmlElement
    private Vak vak;

    @OneToMany(mappedBy = "test", cascade = CascadeType.ALL)
    @XmlElement
    @XmlInverseReference(mappedBy = "test")
    private List<Resultaat> resultaten = new ArrayList<>();

    public Test() {
    }

    public Test(String naam, float maxScore, Vak vak, GregorianCalendar datum) {
        this.maxScore = maxScore;
        this.naam = naam;
        this.datum = datum;
        this.vak = vak;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(float maxScore) {
        this.maxScore = maxScore;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Vak getVak() {
        return vak;
    }

    public void setVak(Vak vak) {
        this.vak = vak;
    }

    public List<Resultaat> getResultaten() {
        return resultaten;
    }

    public void setResultaten(List<Resultaat> resultaten) {
        this.resultaten = resultaten;
    }

    public GregorianCalendar getDatum() {
        return datum;
    }

    public void setDatum(GregorianCalendar datum) {
        this.datum = datum;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Test)) {
            return false;
        }
        Test other = (Test) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return naam;
    }

    public float getGemiddelde() {
        float totaal = 0;
        float aantal = 0;

        for (Resultaat r : getResultaten()) {
            totaal += r.getBehaaldCijfer();
            aantal++;
        }

        return totaal / aantal;
    }

    public float getMediaan() {
        int aantal = getResultaten().size();

        if (aantal % 2 == 0) {
            float mid1 = getResultaten().get(aantal / 2).getBehaaldCijfer();
            float mid2 = getResultaten().get(aantal / 2 - 1).getBehaaldCijfer();
            return (mid1 + mid2) / 2;
        } else {
            return getResultaten().get(aantal / 2).getBehaaldCijfer();
        }

    }

}
