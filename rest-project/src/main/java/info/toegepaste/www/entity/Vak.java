/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.entity;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hendrik
 */
@NamedQueries({
    @NamedQuery(name = "Vak.getAll", query = "select v from Vak v"),
    @NamedQuery(name = "Vak.getByName", query = "select v from Vak v where lower(v.naam) = :vaknaam")
})

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Vak implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String naam;

    @OneToMany(mappedBy = "vak", cascade = CascadeType.ALL)
    @XmlElement
    @XmlInverseReference(mappedBy = "vak")
    private List<Test> testen = new ArrayList<>();

    public Vak() {
    }

    public Vak(String naam) {
        this.naam = naam;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public List<Test> getTesten() {
        return testen;
    }

    public void setTesten(List<Test> testen) {
        this.testen = testen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vak)) {
            return false;
        }
        Vak other = (Vak) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.toegepaste.www.entity.Vak[ id=" + id + " ]";
    }

    public float getGemiddelde() {
        float totaal = 0;
        float aantal = 0;

        for (Test t : getTesten()) {
            totaal += t.getGemiddelde();
            aantal++;
        }

        return totaal / aantal;
    }

    public float getMediaan() {
        int aantal = getTesten().size();

        if (aantal % 2 == 0) {
            float mid1 = testen.get(aantal / 2).getMediaan();
            float mid2 = testen.get(aantal / 2 - 1).getMediaan();
            return (mid1 + mid2) / 2;
        } else {
            return testen.get(aantal / 2).getMediaan();
        }

    }

}
