/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.ejb;

import info.toegepaste.www.entity.Resultaat;
import info.toegepaste.www.entity.Student;
import info.toegepaste.www.entity.Test;
import info.toegepaste.www.entity.Vak;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.FormDataParam;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

/**
 *
 * @author Hendrik Clercx
 */
@Path("resultaten")
@Stateless
public class ResultaatService {

    @PersistenceContext(unitName = "ScoreTrackerPU")
    private EntityManager em;

    @POST
    @Path("uploaden")
    public void leesResultaten(@Context HttpServletRequest request, @Context HttpServletResponse response, @FormDataParam("file") InputStream file) throws IOException, ServletException {
          URL url = new URL(request.getRequestURL().toString());
        String path = url.getProtocol() + "://" + url.getHost() + ":" + url.getPort() + "/project/";
        try{
        List<Resultaat> resultaten = new ArrayList<>();
        boolean juisteFormat = true;

        // Declaratie eigenschappen van het object "test"
        Test test = new Test();
        String naam = "";
        GregorianCalendar datum = new GregorianCalendar();
        Vak vak = new Vak();
        float maxScore = 0;

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        int teller = 0;
        Iterator<Row> rowIterator = sheet.iterator();

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            if (teller <= 4) {
                switch (teller) {
                    case 0:
                        naam = row.getCell(1).toString();
                        break;
                    case 1:
                        //        VAK OPHALEN BY NAAM
                        Query q = em.createNamedQuery("Vak.getByName");
                        q.setParameter("vaknaam", row.getCell(1).toString().toLowerCase());
                        List<Vak> vakken = q.getResultList();
                        if (vakken.size() == 1) {
                            vak = vakken.get(0);
                        } else {
                            juisteFormat = false;
                        }
                        break;
                    case 2:
                        if (row.getCell(1).getCellType() == HSSFCell.CELL_TYPE_NUMERIC && HSSFDateUtil.isCellDateFormatted(row.getCell(1))) {
                            Date date = row.getCell(1).getDateCellValue();
                            date.getDate();
                            datum.setGregorianChange(date);
                        } else {
                            juisteFormat = false;
                        }
                        break;
                    case 3:
                        if (row.getCell(1).getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                            maxScore = (float) row.getCell(1).getNumericCellValue();
                        } else {
                            juisteFormat = false;
                        }
                        break;
                    case 4:
                        test = new Test(naam, maxScore, vak, datum);
                        break;
                    default:
                        break;
                }
            }

            if (teller > 4 && naam != null && vak != null && maxScore != 0) {
                float score = 0;
                Student student;
                List<Student> studenten;

                if (row.getCell(2).getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                    score = (float) row.getCell(2).getNumericCellValue();
                }

                Query q = em.createNamedQuery("Student.getByNames");
                q.setParameter("voornaam", row.getCell(1).toString().toLowerCase());
                q.setParameter("achternaam", row.getCell(0).toString().toLowerCase());
                studenten = q.getResultList();

                if (studenten.size() == 1) {
                    student = studenten.get(0);
                    Resultaat resultaat = new Resultaat(student, test, score);
                    resultaten.add(resultaat);
                } else {
                    juisteFormat = false;
                }
            }

            teller++;
        
        }

        if (juisteFormat == true) {
            try {
                em.persist(test);

                for (Resultaat resultaat : resultaten) {
                    em.persist(resultaat);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            resultaten = null;
        }
      

        if (resultaten == null) {
            response.sendRedirect(path + "error.jsp");
        } else {
            response.sendRedirect(path + "ManageServlet?testresultaten=" + test.getId());
        }
        }catch (Exception e){
         response.sendRedirect(path + "error.jsp");
        }
    }

    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_XML)
    public List<Resultaat> getAllResultaten() {
        return em.createNamedQuery("Resultaat.getAll").getResultList();
    }

    @GET
    @Path("get/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Resultaat getById(@PathParam("id") long id) {
        return em.find(Resultaat.class, id);
    }

    @GET
    @Path("getAllFromStudent/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public List<Resultaat> getAllResultatenStudent(@PathParam("id") long Id) {
        return em.createNamedQuery("Resultaat.getAllFromStudent").setParameter("studentId", Id).getResultList();
    }

    @GET
    @Path("getAllByTest/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public List<Resultaat> getAllResultatenByTest(@PathParam("id") long Id) {
        return em.createNamedQuery("Resultaat.getAllByTest").setParameter("testId", Id).getResultList();
    }

    public List<Resultaat> getAllFromStudentBetweenDates(Long studentId, GregorianCalendar begindatum, GregorianCalendar einddatum) {
        Query q = em.createNamedQuery("Resultaat.getAllFromStudentBetweenDates");
        q.setParameter("studentId", studentId);
        q.setParameter("begindatum", begindatum);
        q.setParameter("einddatum", einddatum);
        return q.getResultList();
    }

    @GET
    @Path("/edit/{id}/{score}")
    public void updateScoreResultaat(@PathParam("id") Long id, @PathParam("score") float score) throws IOException {
        Resultaat resultaat = this.getById(id);
        resultaat.setBehaaldCijfer(score);

        em.persist(resultaat);
    }
}
