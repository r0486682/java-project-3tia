/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.ejb;

import info.toegepaste.www.entity.Vak;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Hendrik Clercx
 */
@Path("vakken")
@Stateless
public class VakService {

    @PersistenceContext(unitName = "ScoreTrackerPU")
    private EntityManager em;

    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_XML)
    public List<Vak> getAllVakken() {
        return em.createNamedQuery("Vak.getAll").getResultList();
    }

   

}
