/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.ejb;

import info.toegepaste.www.entity.Klas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Jordy Dieltjens
 */
@Stateless
@Path("klassen")
public class KlasService {

    @PersistenceContext(unitName = "ScoreTrackerPU")
    private EntityManager em;

    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_XML)
    public List<Klas> getAllClasses() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Klas.class));
        return em.createQuery(cq).getResultList();
        //return em.createNamedQuery("Klas.getAll").getResultList();

    }

    @GET
    @Path("getKlas/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Klas getKlas(@PathParam("id") Long id) {
        String test = "";
        Klas klas = em.find(Klas.class, id);
        return klas;

    }

}
