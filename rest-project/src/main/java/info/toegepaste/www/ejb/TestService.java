package info.toegepaste.www.ejb;

import info.toegepaste.www.beans.PDFWriterImpl;
import info.toegepaste.www.entity.Student;
import info.toegepaste.www.entity.Test;
import info.toegepaste.www.entity.Vak;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author Julie Haesendonckx
 */
@Path("testen")
@Stateless
public class TestService {

    @PersistenceContext(unitName = "ScoreTrackerPU")
    private EntityManager em;

    @POST
    @Path("uploadResultaten")
    public List<Test> leesResultaten(@FormDataParam("file") InputStream file) throws IOException {
        List<Test> testen = new ArrayList<>();
        String naam = "";
        GregorianCalendar datum = new GregorianCalendar();
        Vak vak = new Vak();
        float maxScore = 0;
        boolean juisteFormat = true;

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        int teller = 0;
        Iterator<Row> rowIterator = sheet.iterator();

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            if (teller < 4) {
                switch (teller) {
                    case 0:
                        naam = row.getCell(1).toString();
                        break;
                    case 1:
                        //        VAK OPHALEN BY NAAM
                        Query q = em.createNamedQuery("Vak.getByName");
                        q.setParameter("vaknaam", row.getCell(1).toString().toLowerCase());
                        List<Vak> vakken = q.getResultList();
                        if (vakken.size() == 1) {
                            vak = vakken.get(0);
                        }
                        break;
                    case 2:
                        if (HSSFDateUtil.isCellDateFormatted(row.getCell(1))) {
                            Date test = row.getCell(1).getDateCellValue();
                            test.getDate();
                            datum.setGregorianChange(test);
                        }
                        break;
                    case 3:
                        if (row.getCell(1).getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                            maxScore = (float) row.getCell(1).getNumericCellValue();
                        }
                        break;
                    default:
                        break;
                }
            }

            if (teller > 4 && naam != null && vak != null && maxScore != 0) {
                //testen = inlezenResultaten(testen, naam, datum, vak, maxScore, teller, row);
                float score = 0;
                Student student;
                List<Student> studenten;

                if (row.getCell(2).getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                    score = (float) row.getCell(2).getNumericCellValue();
                }

                Query q = em.createNamedQuery("Student.getByNames");
                q.setParameter("voornaam", row.getCell(1).toString().toLowerCase());
                q.setParameter("achternaam", row.getCell(0).toString().toLowerCase());
                studenten = q.getResultList();

                if (studenten.size() == 1) {
                    student = studenten.get(0);

                } else {
                    juisteFormat = false;
                }
            }

            teller++;
        }

        if (juisteFormat == true) {
            try {
                for (Test test : testen) {
                    em.persist(test);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            testen = null;
        }

        return testen;

    }

    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_XML)
    public List<Test> getAll() {
        return em.createNamedQuery("Test.getAll").getResultList();
    }

    @GET
    @Path("getById/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Test getById(@PathParam("id") long id) {
        return em.find(Test.class, id);
    }

    @GET
    @Path("downloadRapport/{id}")
    @Produces({"application/pdf"})
    public Response downloadRapport(@PathParam("id") long id) throws FileNotFoundException {
        final Test test = getById(id);

        if (test == null) {
            return null;
        }

        // Pdf naar outputstream schrijven
        return Response.ok((StreamingOutput) new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                new PDFWriterImpl().createTestReport(outputStream, test);
            }
        }).header("Content-Disposition", "attachment; filename=Rapport-" + test.getNaam() + ".pdf").build();
    }

    @GET
    @Path("downloadPerodiekRapport/{studentId}/{begindatum}/{einddatum}")
    @Produces({"application/pdf"})
    public Response downloadPerodiekRapport(@PathParam("studentId") long studentId,
            @PathParam("begindatum") final String begindatum,
            @PathParam("einddatum") final String einddatum,
            @PathParam("periode") final String periode) throws FileNotFoundException {
        final Student student = em.find(Student.class, studentId);

        if (student == null) {
            return null;
        }

        //datums van jquery omvormen naar echte datums
        String[] splitBegindatum = begindatum.split("-");
        String[] splitEinddatum = einddatum.split("-");

        final GregorianCalendar begin = new GregorianCalendar();
        begin.set(Integer.parseInt(splitBegindatum[0]), Integer.parseInt(splitBegindatum[1]) - 1, Integer.parseInt(splitBegindatum[2]));
        final GregorianCalendar eind = new GregorianCalendar();
        eind.set(Integer.parseInt(splitEinddatum[0]), Integer.parseInt(splitEinddatum[1]) - 1, Integer.parseInt(splitEinddatum[2]));

        // Pdf naar outputstream schrijven
        return Response.ok((StreamingOutput) new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                new PDFWriterImpl().createPeriodiekReport(outputStream, student, begin, eind, begindatum);
            }
        }).header("Content-Disposition", "attachment; filename=Rapport-" + student.getVoornaam() + "_" + student.getAchternaam() + ".pdf").build();
    }
}
