/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.ejb;

import info.toegepaste.www.beans.CryptoUtil;
import info.toegepaste.www.entity.Gebruiker;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Jordy Dieltjens
 */
@Stateless
@Path("gebruiker")
public class GebruikerService {

    CryptoUtil cryptoUtil = new CryptoUtil();
    String key = "ezeon8547";

    @PersistenceContext(unitName = "ScoreTrackerPU")
    private EntityManager em;

    @GET
    @Path("getPassFromUser/{email}")
    @Produces(MediaType.TEXT_PLAIN)
    public String logIn(@PathParam("email") String email) throws Exception {
        Query q = em.createNamedQuery("Gebruiker.getPassFromUser");
        q.setParameter("email", email);

        List<Gebruiker> gebruikers = q.getResultList();

        if (gebruikers.size() > 0) {
            return cryptoUtil.encrypt(key, gebruikers.get(0).getWachtwoord());
        }
        return "";

    }

}
