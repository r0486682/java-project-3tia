/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.toegepaste.www.ejb;

import info.toegepaste.www.beans.ExcelReaderImpl;
import info.toegepaste.www.entity.Klas;
import info.toegepaste.www.entity.Student;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author Jordy Dieltjens
 */
@Stateless
@Path("studenten")
public class StudentService {

    @PersistenceContext(unitName = "ScoreTrackerPU")
    private EntityManager em;

    @POST
    @Path("uploadStudenten")

    public void leesStudenten(@Context HttpServletRequest request, @Context HttpServletResponse response, @FormDataParam("klaslijst") InputStream file, @FormDataParam("klas") String klasnaam) throws IOException, ServletException {
        URL url = new URL(request.getRequestURL().toString());
        String base_url = url.getProtocol() + "://" + url.getHost() + ":" + url.getPort() + "/project/";
        try {

            Query q = em.createNamedQuery("Klas.getByNaam");
            q.setParameter("naam", klasnaam);
            List<Klas> klassen = q.getResultList();
            if (klassen.size() == 0) {

                ExcelReaderImpl excelReaderImpl = new ExcelReaderImpl();

                List<Student> studenten = excelReaderImpl.readStudents(file);
                if (studenten != null) {
                    Klas klas = new Klas(klasnaam);
                    klas.setStudenten(studenten);

                    //persit maken
                    storeStudents(studenten, klas);

                    response.sendRedirect(base_url + "ManageServlet?klastoegevoegd=" + klas.getId());
                } else {
                    response.sendRedirect(base_url + "lijstUploaden.jsp?error=true");
                }

            } else {
                response.sendRedirect(base_url + "lijstUploaden.jsp?error=klasnotfound");
            }
        } catch (Exception e) {
            response.sendRedirect(base_url + "error.jsp");
        }
    }

    public List<Student> storeStudents(List<Student> studenten, Klas klas) {
        try {
            em.persist(klas);
            for (Student student : studenten) {
                student.setKlas(klas);
                em.persist(student);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return studenten;
    }

    @GET
    @Path("getAll")
    public List<Student> getAllStudents() {
        List<Student> studenten = em.createNamedQuery("Student.getAll").getResultList();
        return studenten;
    }

}
